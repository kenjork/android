package kenjork.practica;


import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 */
public class DetallePedidoFragment extends Fragment {
    TextView texto;

    public DetallePedidoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_detalle_pedido, container, false);
/*
        texto = (TextView) rootView.findViewById((R.id.tvTexto));

        Bundle datosRecibidos = getArguments();
        texto.setText(datosRecibidos.getString("codigo"));*/


        Bundle datosRecibidos = getArguments();
        String codigo = datosRecibidos.getString("codigo");

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        String ruta = "http://192.168.0.108:8080/android/detallePedidos.php";


        try {
            URL url = new URL(ruta);//Asi el string se interpreta como una
            HttpURLConnection httpURLConnection =(HttpURLConnection)url.openConnection();
            httpURLConnection.setRequestMethod("POST");

            Uri.Builder builder = new Uri.Builder().appendQueryParameter("CodigoPedido",codigo);

            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(httpURLConnection.getOutputStream()));

            bufferedWriter.write(builder.build().getEncodedQuery());
            bufferedWriter.flush();

            InputStream inputStream = new BufferedInputStream(httpURLConnection.getInputStream());
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));


            JSONArray jsonArray = new JSONArray(bufferedReader.readLine());
            final ArrayList<HashMap<String,String>> arrayList = new ArrayList<>();
            for (int i =0;i<=jsonArray.length()-1;i++){
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                HashMap<String,String> map = new HashMap<>();
                map.put("idPedido", jsonObject.getString("idpedido"));
                map.put("idProducto", jsonObject.getString("idproducto"));
                map.put("precio", jsonObject.getString("precio"));
                map.put("cantidad", jsonObject.getString("cantidad"));
                arrayList.add(map);
            }

            final ListView mlvPedido =(ListView)rootView.findViewById(R.id.lvDetallePedidos);
            ListAdapter listAdapter = new SimpleAdapter(getActivity(),
                    arrayList,R.layout.item_detalle,
                    new String[]{"idProducto","precio","cantidad"},
                    new int[]{R.id.tvPedido,R.id.tvEntrega,R.id.tvDestinatario});

            mlvPedido.setAdapter(listAdapter);






        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return rootView;






    }

}
