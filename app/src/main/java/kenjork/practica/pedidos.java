package kenjork.practica;

import android.content.Intent;
import android.net.Uri;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

public class pedidos extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedidos);


        Bundle datosRecibidos = getIntent().getExtras();
        this.setTitle(datosRecibidos.getString("nombre"));
        String codigo = datosRecibidos.getString("codigo");

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        String ruta = "http://192.168.0.108:8080/android/pedidos.php";

        try{
            URL url = new URL(ruta);//Asi el string se interpreta como una
            HttpURLConnection httpURLConnection =(HttpURLConnection)url.openConnection();

            httpURLConnection.setRequestMethod("POST");

            Uri.Builder builder = new Uri.Builder().appendQueryParameter("CodigoEmp",codigo);

            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(httpURLConnection.getOutputStream()));

            bufferedWriter.write(builder.build().getEncodedQuery());
            bufferedWriter.flush();


            InputStream inputStream = new BufferedInputStream(httpURLConnection.getInputStream());
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));


            JSONArray jsonArray = new JSONArray(bufferedReader.readLine());
            final ArrayList<HashMap<String,String>> arrayList = new ArrayList<>();
            for (int i =0;i<=jsonArray.length()-1;i++){
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                HashMap<String,String> map = new HashMap<>();
                map.put("cod", jsonObject.getString("idpedido"));
                map.put("fPedido", jsonObject.getString("fechapedido"));
                map.put("fEntrega", jsonObject.getString("fechaentrega"));
                map.put("dest", jsonObject.getString("destinatario"));
                arrayList.add(map);
            }

            final ListView mlvPedido =(ListView)findViewById(R.id.lvPedidos);
            ListAdapter listAdapter = new SimpleAdapter(this,
                    arrayList,R.layout.item_pedidos,
                    new String[]{"fPedido","fEntrega","dest"},
                    new int[]{R.id.tvPedido,R.id.tvEntrega,R.id.tvDestinatario});

            mlvPedido.setAdapter(listAdapter);


            mlvPedido.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    HashMap<String,String> map = arrayList.get(i);
                   /* String dest = map.get("dest");*/
                    String cod = map.get("cod");
                    Toast.makeText(getApplicationContext(),cod,Toast.LENGTH_SHORT).show();

                    Bundle datos =new Bundle();
                    datos.putString("codigo",cod);
                    /*datos.putString("nombre",dest);*/

                    DetallePedidoFragment detallePedidoFragment = new DetallePedidoFragment();
                    detallePedidoFragment.setArguments(datos);
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.contenedor,detallePedidoFragment)
                            .commit();


                   /* Intent intent = new Intent(getApplicationContext(),pedidos.class);
                    intent.putExtras(datos);
                    startActivity(intent);*/

                }
            });




        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }
}
